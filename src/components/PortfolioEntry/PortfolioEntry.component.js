import React from "react";
import { Icon, Image } from "semantic-ui-react";

import "./PortfolioEntry.styles.css";

const PortfolioEntry = (props) => {
  return (
    <div className={"entry"}>
      <Image className={"projectLogo"} src={props.imageUrl} />
      <div className={"entryContent"}>
        <span>
          <label>{props.entryTitle}</label>
          <br />
          <br />
          {props.description}
        </span>
        <br />
        <a href={props.siteUrl} target="_blank">
          <Icon name={"file"} />
          Finished Website
        </a>
        <br />
        <a href={props.gitUrl} target="_blank">
          <Icon name={"git"} />
          GIT Repository
        </a>
      </div>
    </div>
  );
};

export default PortfolioEntry;
