import React, { useState } from "react";
import { Menu, Icon } from "semantic-ui-react";
import { Link } from "react-router-dom";

import "./NavBar.styles.css";

const NavBar = () => {
  const [activeItem, setActiveItem] = useState("home");

  const handleItemClick = (e, { name }) => setActiveItem(name);

  return (
    <div className={"navBar"}>
      <Menu className={"menuNormal"} color={"violet"} pointing secondary>
        <Menu.Item
          as={Link}
          to="/"
          name="home"
          active={activeItem === "home"}
          onClick={handleItemClick}
        >
          <Icon name={"home"} /> Home
        </Menu.Item>
        <Menu.Item
          as={Link}
          to="/about"
          name="about"
          active={activeItem === "about"}
          onClick={handleItemClick}
        >
          <Icon name={"user"} /> About
        </Menu.Item>
        <Menu.Item
          as={Link}
          to="/work"
          name="work"
          active={activeItem === "work"}
          onClick={handleItemClick}
        >
          <Icon name={"building"} /> Work
        </Menu.Item>
        <Menu.Item
          as={Link}
          to="/portfolio"
          name="portfolio"
          active={activeItem === "portfolio"}
          onClick={handleItemClick}
        >
          <Icon name={"suitcase"} /> Portfolio
        </Menu.Item>
        <Menu.Item
          as={Link}
          to="/hobby"
          name="hobby"
          active={activeItem === "hobby"}
          onClick={handleItemClick}
        >
          <Icon name={"shield"} /> Hobby
        </Menu.Item>
        <br />
      </Menu>
      <Menu className={"menuMobile"} color={"violet"} pointing secondary>
        <Menu.Item
          as={Link}
          to="/"
          name="home"
          active={activeItem === "home"}
          onClick={handleItemClick}
        >
          <Icon name={"home"} /> Home
        </Menu.Item>
        <Menu.Item
          as={Link}
          to="/about"
          name="about"
          active={activeItem === "about"}
          onClick={handleItemClick}
        >
          <Icon name={"user"} /> About
        </Menu.Item>
        <Menu.Item
          as={Link}
          to="/work"
          name="work"
          active={activeItem === "work"}
          onClick={handleItemClick}
        >
          <Icon name={"building"} /> Work
        </Menu.Item>
        <Menu.Item
          as={Link}
          to="/portfolio"
          name="portfolio"
          active={activeItem === "portfolio"}
          onClick={handleItemClick}
        >
          <Icon name={"suitcase"} /> Portfolio
        </Menu.Item>
        <Menu.Item
          as={Link}
          to="/hobby"
          name="hobby"
          active={activeItem === "hobby"}
          onClick={handleItemClick}
        >
          <Icon name={"shield"} /> Hobby
        </Menu.Item>
        <br />
      </Menu>
    </div>
  );
};

export default NavBar;
