import React from "react";
import { Container, Image, Segment } from "semantic-ui-react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { AnimatedSwitch } from "react-router-transition";

import NavBar from "./components/NavBar/NavBar.component";
import Home from "./pages/Home/Home.page";
import About from "./pages/About/About.page";
import Hobby from "./pages/Hobby/Hobby.page";
import Work from "./pages/Work/Work.page";
import Portfolio from "./pages/Portfolio/Portfolio.page";

import "./App.css";

function App() {
  return (
    <Router>
      <Container className={"rootContainer"}>
        <Segment className={"main centered"}>
          <div className={"topBar"}>
            <NavBar />
            <Image
              className={"logo"}
              src={"https://i.imgur.com/NCJLZsY.png"}
              floated={"right"}
            />
          </div>
          <div className={"app"}>
            <Switch>
              <AnimatedSwitch
                atEnter={{ opacity: 0 }}
                atLeave={{ opacity: 0 }}
                atActive={{ opacity: 1 }}
                className="switch-wrapper"
              >
                <Route exact path="/">
                  <Home />
                </Route>
                <Route path="/about">
                  <About />
                </Route>
                <Route path="/work">
                  <Work />
                </Route>
                <Route path="/portfolio">
                  <Portfolio />
                </Route>
                <Route path="/hobby">
                  <Hobby />
                </Route>
              </AnimatedSwitch>
            </Switch>
          </div>
        </Segment>
      </Container>
    </Router>
  );
}

export default App;
