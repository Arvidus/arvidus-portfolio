import React from "react";
import { Header, Image } from "semantic-ui-react";

const Work = () => {
  return (
    <>
      <Header>
        <h1>Work</h1>
      </Header>
      <p>
        In 2012, a short one-year course in Game Design led me to study three
        years of Game Design and -Programming (<b>C++</b> and <b>C#</b>) at
        Uppsala University, Campus Gotland, Visby. There I also had the
        opportunity to be introduced to Webdesign and -Programming (<b>HTML</b>,{" "}
        <b>CSS</b> and <b>JavaScript</b>).
      </p>
      <p>
        I took an interest to the field and continued my studies at Nackademin
        Yrkeshögskola (Polytechnic) as a <b>Java</b>-developer. Among the
        courses offered, I took a particular interest in <b>Frontend</b>
        -development and focused on this aspect during my second year
        internships and my master thesis:
      </p>
      <p>
        <Image
          src={"https://i.imgur.com/VGTrnNP.png"}
          floated={"left"}
          size={"medium"}
        />
        Three months was spent at the web bureau iResponse Network & Media AB,
        Stockholm, where several monumentous tasks and challenges were presented
        to me. Despite many difficulties, challenging and tedious, a lot was
        learned and valuable experience was gained in general web-development
        (HTML, CSS, Javascript, <b>PHP</b> and <b>Bootstrap</b>).
      </p>
      <br />
      <p>
        <Image src={"https://i.imgur.com/vY5DPPZ.png"} floated={"right"} />
        Three additional months was spent at the consultancy BiDirection
        Consulting AB, Stockholm, where experience in team-based development was
        cemented (with previous experience from Campus Gotland), via good and
        bad examples. Additional challenge was presented by the fact that the
        team was divided between locations, which complicated communication and
        coordination within the team. It was here that I was first introduced to
        one of the three major JavaScript-based Frontend-frameworks (
        <b>Angular</b>), which prompted me to study and practice further with
        the other two (<b>ReactJS</b> and <b>Vue.js</b>) as well as basing my
        master thesis on a comparative analysis between two of them (Angular and
        ReactJS).
      </p>
      <p>
        <Image
          src={"https://i.imgur.com/wHh1jwj.png"}
          floated={"left"}
          size={"medium"}
        />
        Right now, since late 2019, I works with various projects and
        assignments at the consultancy Combitech AB, Bromma, Stockholm and put
        my focus primarily on Frontend web-development, hoping to continue in
        this direction of occupation, to eventually also include <b>Backend</b>
        -development to make myself a <b>Fullstack</b> developer.
      </p>
    </>
  );
};

export default Work;
