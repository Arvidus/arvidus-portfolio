import React from "react";
import { Header } from "semantic-ui-react";

import "./Home.styles.css";

const Home = () => {
  return (
    <div>
      <Header>
        <h2>
          Gustav "Arvidus" Hässler <br />
          Portfolio Website
        </h2>
      </Header>
      <p>
        Welcome to the personal website of Gustav "Arvidus" Hässler, which is
        dedicated to showcase said individual's previous work; -professional and
        -personal.
      </p>
    </div>
  );
};

export default Home;
