import React from "react";
import { Header, Image } from "semantic-ui-react";

import "./About.styles.css";

const About = () => {
  return (
    <>
      <Header>
        <h1>About</h1>
      </Header>
      <p>
        <Image
          className={"photo"}
          src={"https://i.imgur.com/ZP7JEBp.png"}
          floated={"left"}
        />
        I've always considered myself a creative individual and have spent much
        time finding different mediums to express said creativity, gaining
        experience in a variety of different arts and crafts. <br />
      </p>
      <p>
        Among other things, I enjoy especially to work with graphical material,
        which has lead me to work on the more visual aspect of programming and
        contribute to smaller game-projects on my free time.
      </p>
      <p>
        I'm stubborn when it comes to any kind of work. Once I've begun in my
        efforts to complete a task, I'll continue until I'm done, often with
        disregard for breaks and interruptions, both to a fault and a strength.
      </p>
      <p>
        I never shy away from challenges or learning new techniques, while
        always striving to improve my current ones.
      </p>
    </>
  );
};

export default About;
