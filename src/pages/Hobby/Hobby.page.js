import React from "react";
import { Divider, Header, Image } from "semantic-ui-react";

import "./Hobby.styles.css";

const Hobby = () => {
  return (
    <>
      <Header>
        <h1>Hobby</h1>
      </Header>
      <p>
        <figure className={"coaFigure"}>
          <Image
            className={"coatOfArms"}
            src={"https://i.imgur.com/916mLWy.png"}
          />
          <figcaption>My Personal Coat of Arms</figcaption>
        </figure>
        I've has always had a passion for visual design and -work. At start of a
        young age, I was particularly interested in design and knowledge
        surrounding <b>heraldry</b> (coats of arms and shields),
        <b>vexillology</b> (flags and banners) and general symbols. I find it
        fascinating how a single piece of art, abstract or detailed can be
        designed to represent a person, a family or a whole nation's people,
        history and culture. It has led me to create my own pieces within the
        category, for myself and others online via request. <br />
        This also compliments my other interest, as it serves to create visual
        bookmarks throughout <b>history</b>.
      </p>
      <p>
        Even if <b>game design</b> is no longer my focus as a career, it still
        serves as another good hobby of mine. I contribute{" "}
        <b>graphical material</b> mostly via my knowledge in{" "}
        <b>Adobe Photoshop</b> to several independent game projects and game
        modification projects. The most prominent being that of a game
        modification for Hearts of Iron 4 by Paradox Interactive; <br />
        Kaiserreich: Legacy of the Weltkrieg. Considered its own stand-alone
        game, it takes place in an alternate history scenario during the
        1930s-50s where Germany and the Central Powers were victorious in the
        First World War, making the German Empire the world's dominant political
        power.
      </p>
      <p>
        <figure className={"gameFigure"}>
          <Image src={"https://i.imgur.com/OqdgUQE.png"} />
          <figcaption>Military Branch Icons</figcaption>
        </figure>
        My contribution to the game is that of graphical UI icons that represent
        political actions and decisions the player takes on as the leader of one
        of the world's nations.
      </p>
      <br />
      <Divider />
      <div>
        <label>
          <u>
            <b>Links to galleries, showcasing more of my graphical work:</b>
          </u>
        </label>
        <div className={"galleries"}>
          <Image
            className={"galleryLink"}
            onClick={() => window.open("https://imgur.com/a/ziZw3oF", "_blank")}
            src={"https://i.imgur.com/Fcig2Na.png"}
          />
          <Image
            className={"galleryLink"}
            onClick={() => window.open("https://imgur.com/a/cDOLUkg", "_blank")}
            src={"https://i.imgur.com/uGOwArh.png"}
          />
          <Image
            className={"galleryLink"}
            onClick={() => window.open("https://imgur.com/a/ftSITNz", "_blank")}
            src={"https://i.imgur.com/VWLUu8T.png"}
          />
        </div>
      </div>
    </>
  );
};

export default Hobby;
