import React from "react";
import { Header } from "semantic-ui-react";

import PortfolioEntry from "../../components/PortfolioEntry/PortfolioEntry.component";

const Portfolio = () => {
  return (
    <>
      <Header>
        <h1>Portfolio</h1>
      </Header>
      <div>
        <PortfolioEntry
          imageUrl={"https://i.imgur.com/8DdZnJc.png"}
          entryTitle={"SpaceX - Launch List"}
          description={
            "A test project. Using SpaceX's public API, loads a list of past and future launches of rockets."
          }
          siteUrl={"https://spacex-launch-lists.now.sh/"}
          gitUrl={"https://bitbucket.org/Arvidus/spacex-launch-lists/"}
        />
        <PortfolioEntry
          imageUrl={"https://i.imgur.com/1q3fA5K.png"}
          entryTitle={"MapBox - Admin Unit Finder"}
          description={
            "A test project. Using the MapBox GPS API, Wikipedia API (Wikimedia) and GeoNames reverse " +
            "geocoding API. Can identify any region, province or municipality within Sweden via coordinates."
          }
          siteUrl={"https://mapbox-admin-unit-finder.now.sh/"}
          gitUrl={"https://bitbucket.org/Arvidus/mapbox-admin-unit-finder"}
        />
        <PortfolioEntry
          imageUrl={"https://i.imgur.com/6JcFLRx.png"}
          entryTitle={"Vinnovera - Weather"}
          description={
            "A test project. Using the Open Weather Map API, identifies location names and reads weather " +
            "data to present weather and temperature."
          }
          siteUrl={"https://vinnovera-weather.now.sh/"}
          gitUrl={"https://bitbucket.org/Arvidus/vinnovera-weather/"}
        />
        <PortfolioEntry
          imageUrl={"https://i.imgur.com/jifi9ex.png"}
          entryTitle={"Decathlon Judge"}
          description={
            "A test project. Designed as a tool for judges in decathlon competitions to calculate points " +
            "and overall score for each competitor."
          }
          siteUrl={"https://decathlon-judge.now.sh/"}
          gitUrl={"https://bitbucket.org/Arvidus/decathlon-judge/"}
        />
      </div>
    </>
  );
};

export default Portfolio;
